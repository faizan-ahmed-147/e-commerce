import HeroSection from "../components/HeroSection";
import { useProductContext } from "../context/productcontex";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

const About = () => {

  const navigate = useNavigate();
  const { myName } = useProductContext();

  const data = {
    name: "ECommerce Store",
  };

  const verifyPage=async()=>{

    try{
      const res=await fetch('/about',{
        method:"GET",
        headers:{
            Accept:"application/json",
            "Content-Type":"application/json"
        },
        credentials:"include"
    });
    const data=await res.json();
    console.log(data);
    // setuserData(data);
    if(!res.status===200){
        const err=new Error(res.error);
        throw err;
    }
}catch(err) {
    console.log(err);
    navigate("/login");
}
}

useEffect(()=>{
verifyPage();
},[])


  return (
    <>
      {myName}
      <HeroSection myData={data} />
    </>
  );
};

export default About;
