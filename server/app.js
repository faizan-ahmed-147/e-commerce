const express = require('express')

const app = express()
const cookieParser = require('cookie-parser');
const connectToMongo = require ("./db/conn");
connectToMongo();
const dotenv = require("dotenv")
dotenv.config({ path: './config.env'})

app.use(express.json());
app.use(cookieParser())
app.use(require('./router/auth'))



// heruku setup 
const PORT = process.env.PORT || 5000;

app.use(express.static(path.join(__dirname, "./frontend/build")));

app.get("*", function (_, res) {
  res.sendFile(
    path.join(__dirname, "./frontend/build/index.html"),
    function (err) {
      if (err) {
        res.status(500).send(err);
      }
    }
  );
});

// app.get("/about", middleware,(req, res) => {
//     res.send(`Hello world about`);
// });
// app.get("/contact", (req, res) => {
    
//     res.send(`Hello world contact`);
// });

// app.get("/register", (req, res) => {
//     res.send(`Hello world register`);
// });
// app.get("/signin",(req, res) => {
//     res.send(`Hello world signin`);
// });

app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}`)
  })
  